require 'debugger'
require 'colorize'

class InvalidMoveError < StandardError
end

class Checkers

  attr_accessor :board

  def initialize
    @board = Board.new
  end

  def play
    turn = [:red, :black]
    until player_dead?(:red) || player_dead?(:black)
      board.display

      puts "It is #{turn.first}'s turn"
      puts "'1' for one move, '2' for multiple moves"
      move_type = gets.chomp.to_i


      print "Move from which x-coordinate?: "
      x = gets.chomp.to_i
      print "Move from which y-coordinates?: "
      y = gets.chomp.to_i

      move_from = [x,y]

      move_to = []
      if move_type == 1
        print "Move to which x-coordinate?: "
        x = gets.chomp
        print "Move to which y-coordinate?: "
        y = gets.chomp
        move_to << [x.to_i,y.to_i]
      else
        while true
          print "Move to which x-coordinate?: "
          x = gets.chomp
          break if x == "done"
          print "Move to which y-coordinate?: "
          y = gets.chomp
          move_to << [x.to_i,y.to_i]
        end
      end

      board[move_from].perform_moves(move_to)

      turn.reverse!
    end

    print "Game over!"
  end

  def player_dead?(color)
    board.pieces.none? { |piece| piece.color == color }
  end

end

class Board

  attr_accessor :pieces, :grid

  def initialize(set_board = true)
    @pieces = []
    @grid = create_grid
    create_and_set_pieces unless set_board == false
  end

  def create_grid
    grid = Array.new(8) { Array.new(8) }
  end

  def show_pieces
    @pieces.each do |piece|
      puts "#{piece.color} / #{piece.position}"
    end
  end

  def create_and_set_pieces
    # set black
    # create first row pieces
    (0..7).each do |col|
      next unless col % 2 == 0
      pieces << Piece.new(:black, [0,col], self)
      grid[0][col] = pieces.last
    end

    # create second row pieces
    (0..7).each do |col|
      next unless col % 2 != 0
      pieces << Piece.new(:black, [1,col], self)
      grid[1][col] = pieces.last
    end

    # create third row pieces
    (0..7).each do |col|
      next unless col % 2 == 0
      pieces << Piece.new(:black, [2,col], self)
      grid[2][col] = pieces.last
    end

    # set red
    # create first row pieces
    (0..7).each do |col|
      next unless col % 2 != 0
      pieces << Piece.new(:red, [7,col], self)
      grid[7][col] = pieces.last
    end

    # create second row pieces
    (0..7).each do |col|
      next unless col % 2 == 0
      pieces << Piece.new(:red, [6,col], self)
      grid[6][col] = pieces.last
    end

    # create third row pieces
    (0..7).each do |col|
      next unless col % 2 != 0
      pieces << Piece.new(:red, [5,col], self)
      grid[5][col] = pieces.last
    end
    nil
  end

  def [](pos)
    @grid[pos.first][pos.last]
  end

  def []=(pos, value)
    @grid[pos.first][pos.last] = value
  end

  def display

    index = 1
    grid.each_with_index do |row, row_index|
      print row_index
      row.each do |square|
        if index % 2 == 0
          print "   ".colorize(:background => :light_black) if square.nil?
          print " #{square.icon} ".colorize(:background => :light_white) if square
        else
          print "   ".colorize(:background => :light_white) if square.nil?
          print " #{square.icon} ".colorize(:background => :light_white) if square
        end
        index += 1
      end
      index += 1
      puts
    end
    print "  0  1  2  3  4  5  6  7"
    puts

    nil
  end

  def dup
    dup_board = Board.new(set_board=false)
    
    pieces.each do |piece|
      dup_board.pieces << piece.dup(dup_board)
    end

    dup_board.pieces.each do |new_piece|
      dup_board.grid[new_piece.position[0]][new_piece.position[1]] = new_piece
    end
    
    dup_board
  end

end

class Piece

  attr_accessor :king, :position
  attr_reader :color, :board, :forward_shift, :icon

  def initialize(color, position, king=false, board)
    @king = king
    @color = color
    @position = position
    @board = board #board object
    if color == :red
      @forward_shift = -1
      @icon = "\u2659"
    else
      @forward_shift = 1
      @icon = "\u265F"
    end
  end

  def dup(dup_board)
    self.class.new(color, position.dup, dup_board)
  end

  def perform_moves(move_sequence)
    if valid_move_seq?(move_sequence)
      perform_moves!(move_sequence)
    end
  end

  def perform_moves!(move_sequence)
    if move_sequence.count == 1
      move = move_sequence.first
      if move_slide?(move)
        perform_slide(move)
      else
        perform_jump(move)
      end
    else
      move_sequence.each do |coordinates|
        perform_jump(coordinates)
      end
    end
  end

  def move_slide?(move)
    (move[0]-position[0]).abs < 2
  end

  def valid_move_seq?(move_sequence)
    dup_board = board.dup
    dup_piece = self.dup(dup_board)
    begin
      dup_piece.perform_moves!(move_sequence)
    rescue InvalidMoveError => e
      return false
    else
      return true
    end
  end

  def on_board?(coordinates)
    return true if (0..7).include?(coordinates[0]) && (0..7).include?(coordinates[1])
    false
  end

  def perform_slide(move_coordinates)
    possible_slide_moves = slide_moves
    eligible_slide_moves = []

    possible_slide_moves.each do |coordinates|
      if board[coordinates].nil? && on_board?(coordinates)
        eligible_slide_moves << coordinates
      end
    end

    if eligible_slide_moves.include?(move_coordinates)
      board[position] = nil
      board[move_coordinates] = self
      @position = move_coordinates
    else
      raise InvalidMoveError.new "That is an invalid move!"
    end

    nil
  end

  def perform_jump(move_coordinates)
    starting_position = position.dup
    eligible_jump_moves = []
    adjacent_opponents_coords = []

    opponent_x = (position[0]+move_coordinates[0])/2
    opponent_y = (position[1]+move_coordinates[1])/2
    opponent = [opponent_x,opponent_y]

    jump_moves.each do |coordinates|
      if on_board?(coordinates) && board[coordinates].nil? && !(board[opponent].nil?) && board[opponent].color != color
        eligible_jump_moves << coordinates
      end
    end

    if eligible_jump_moves.include?(move_coordinates)
      board[position] = nil
      board[move_coordinates] = self
      self.position = move_coordinates
      board.pieces.delete(board[opponent])
      board[opponent] = nil
    else
      raise InvalidMoveError.new "That is an invalid move!"
    end
    nil
  end

  def slide_moves
    possible_slide_moves = []

    possible_slide_moves << [position[0]+forward_shift,position[1]+1]
    possible_slide_moves << [position[0]+forward_shift,position[1]-1]

    possible_slide_moves
  end

  def jump_moves
    coordinates_to_check = []

    coordinates_to_check << [position[0]+(2*forward_shift),position[1]+2]
    coordinates_to_check << [position[0]+(2*forward_shift),position[1]-2]

    coordinates_to_check
  end
end